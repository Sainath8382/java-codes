//10
//10 9
//9 8 7
//7 6 5 4

class Demo{

	public static void main(String[] s){
		
		int rows=4;
		int n=rows*(rows+1)/2;

		for(int i=1;i<=rows;i++){

			for(int j=1;j<=i;j++){

				System.out.print(n+"\t");
				n--;
			}
			n++;
			System.out.println();
		}
	}
}
