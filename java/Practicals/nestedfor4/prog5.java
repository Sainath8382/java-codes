//A B C D
//B C D
//C D
//D

class Demo{

	public static void main(String[] s){

		int rows=4;

		for(int i=1;i<=rows;i++){

			char ch=(char)(64+i);

			for(int j=rows;j>=i;j--){

				System.out.print(ch+" ");
				ch++;
			}
			
			System.out.println();
		}
	}
}
