//1 2 3 4
//2 3 4
//3 4
//4

class Demo{

	public static void main(String[] s){
		
		int rows=4;

		for(int i=1;i<=rows;i++){
			
			int no=i;

			for(int j=1;j<=i;j++){

				System.out.print(no+" ");
				no++;
			}

			System.out.println();
		}
	}
}
