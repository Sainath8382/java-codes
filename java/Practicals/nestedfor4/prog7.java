//F
//E 1
//D 2 E
//C 3 D 4
//B 5 C 6 D
//A 7 B 8 C 9

class Demo{

	public static void main(String[] s){

		int rows=6;
		char ch=(char)(64+rows);
		int no=1;
		for(int i=1;i<=rows;i++){
			
			char ch1=ch;

			for(int j=1;j<=i;j++){
				
				if(j%2!=0){

					System.out.print(ch1+" ");
					ch1++;
				}else{
					System.out.print(no+" ");
					no++;
				}
			}
			System.out.println();
			ch--;
		}
	}
}


