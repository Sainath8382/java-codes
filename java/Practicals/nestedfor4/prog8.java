//10
//I H
//7 6 5
//D C B A

class Demo{

	public static void main(String[] s){

		int rows=4;
		int no=rows*(rows+1)/2;
		char ch=(char)(64+no);

		for(int i=1;i<=rows;i++){

			for(int j=1;j<=i;j++){

				if(i%2!=0)
					System.out.print(no+"\t");
				else
					System.out.print(ch+"\t");
				no--;
				ch--;
			}
			
			System.out.println();
		}
	}
}


