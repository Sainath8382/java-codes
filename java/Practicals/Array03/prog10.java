//print Armstrong number in array 

import java.io.*;

class Array{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++)
			arr[i] = Integer.parseInt(br.readLine());

		int flag = 0;
		for(int i=0;i<arr.length;i++){
			
			boolean no = isarmstrong(arr[i]);
			if(no == true){
				flag=1;
				System.out.println("Armstrong number = "+arr[i]);
			}
		}
		if(flag ==0)
			System.out.println("No armstrong number in array");
	}


	static boolean isarmstrong(int num){

		int temp = num;
		int dcount = 0;

		int temp1=num;
		while(temp!=0){
			dcount++;
			temp=temp/10;
		}

		int sum=0;
		while(temp1!=0){

			int digit=temp1%10;
			int pro =1;
			for(int i=1;i<=dcount;i++)
				pro=pro*digit;

			sum=sum+pro;
			temp1=temp1/10;
		}

		if(sum==num)
			return true;
		else
			return false;
	}
}

