//Program 9
//Write a program to print the second max element in the array
//Input: Enter array elements: 2 255 2 1554 15 65
//Output: 255

import java.io.*;

class Array{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Array size ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("elements:");
		for(int i=0;i<arr.length;i++)
			arr[i]= Integer.parseInt(br.readLine());


		if(size>2){
			for(int i=0;i<arr.length;i++){
			
				for(int j=i+1;j<arr.length;j++){

					if(arr[i]>arr[j]){
						int temp = arr[i];
						arr[i] = arr[j];
						arr[j] = temp;
					}
				}
			}
		}else{
			System.out.println("You entered size less than 2");
		}

		System.out.println("Second largest element is "+ arr[arr.length-2]);

	}
}
