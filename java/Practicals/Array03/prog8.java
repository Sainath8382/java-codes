//return the index of palindrome number in the given array

import java.io.*;

class Array{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int count = 0;
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			count++;

			if(count == 0)
				System.out.println("Array is empty");
		}
		for(int i=0;i<arr.length;i++){
			boolean ispal = ispal(arr[i]);
			if(ispal == true)
				System.out.println("Palindrome found at index : "+i);
		}
	}

	static boolean ispal(int num){

		int temp  = 0;
		int temp1= num;

		while(temp1!=0){
			temp = temp*10+temp1%10;
			temp1=temp1/10;
		}

		if(temp==num)
			return true;
		else
			return false;
	}
}


