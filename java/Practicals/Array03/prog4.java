//wap to reverse each element in array

import java.io.*;

class Array{

	int reverse(int num){

		int temp =0;
		while(num != 0){
			temp = temp*10+num%10;
			num= num/10;
		}

		return temp;
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Array size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Elements ?");
	
		int count = 0;
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			count++;
		}

		if(count>0){
		for(int i=0;i<arr.length;i++){
			Array obj = new Array();
			int rev = obj.reverse(arr[i]);

			arr[i] = rev;
		}

		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+"\t");

		System.out.println();
		}else{
			System.out.println("Given array is empty");
		}

	}

}


	
