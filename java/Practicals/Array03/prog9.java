//Print Strong number from array

import java.io.*;

class Array{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Elements ");
		for(int i=0;i<arr.length;i++)
			arr[i] = Integer.parseInt(br.readLine());

		int flag = 0;
		for(int i=0;i<arr.length;i++){

			boolean no = strong(arr[i]);
			if(no == true){
				flag = 1;
				System.out.println("STrong number : "+arr[i]);
			}
		}
		if(flag == 0)
			System.out.println("No perfect number in array");
	}
	
	static boolean strong(int num){

		int temp = num;
		int sum = 0;

		while(temp!=0){

			int no = temp%10;
			int fact =1;
			for(int i=1;i<=no;i++){
				fact = fact*i;
			}
			sum = sum+fact;
			temp = temp/10;
		}

		if(sum == num)
			return true;
		else
			return false;
	}
}
