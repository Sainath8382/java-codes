//Program 10
//WAP to print the elements whose addition of digits is even.
//Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
//Input :
//Enter array : 1 2 3 5 15 16 14 28 17 29 123
//Output: 2 15 28 17 123

import java.io.*;

class Array{

	public static void main(String[] s)throws IOException{

		BufferedReader br = new  BufferedReader(new InputStreamReader(System.in));
		System.out.println("ENter size of array");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
	
	System.out.println();

	for(int i=0;i<arr.length;i++){
		int sum = 0;
			int temp = arr[i];
			while(temp!=0){

				int digit = temp%10;
				sum = sum+digit;
				temp = temp/10;
			
			
			}
			if(sum%2==0)
				System.out.println(arr[i]);
			
		}
	}
}




