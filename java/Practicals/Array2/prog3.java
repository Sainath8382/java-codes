//Program 3
//Program 3
//Write a Java program to find the sum of even and odd numbers in an array.
//Display the sum value.
//Input: 11 12 13 14 15
//Output
//Odd numbers sum = 39
//Even numbers sum = 26

import java.io.*;

class Array{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size: ");

		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		int s1=0,s2=0;
		System.out.println("Enter array elements: ");
		for(int i=0;i<arr.length;i++){

			arr[i] = Integer.parseInt(br.readLine());

			if(arr[i] %2==0)
				s1 = s1+arr[i];
			else
				s2 = s2+arr[i];
		}

		System.out.println("Even elements sum : "+s1);
		System.out.println("odd elements sum : "+s2);
	}
}



























