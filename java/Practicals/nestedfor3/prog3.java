//10
//9 8
//7 6 5
//4 3 2 1

class Demo{

	public static void main(String[] s){

		int rows=4,n = rows*(rows+1)/2;

		for(int i=1;i<=rows;i++){

			for(int j=1;j<=i;j++){

				System.out.print(n+"\t");
				n--;
			}

			System.out.println();
		}
	}
}
