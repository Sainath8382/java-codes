//J
//I H
//G F E
//D C B A

class Demo{

	public static void main(String[] s){

		int rows=4;
		char ch= (char)(64+(rows*(rows+1)/2));

		for(int i=1;i<=rows;i++){

			for(int j=1;j<=i;j++){

				System.out.print(ch+" ");
				ch--;
			}
			System.out.println();
		}
	}
}
