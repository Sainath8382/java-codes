//Automorphic number is a number in which the last digit of the square of the number is equal to the number itself is
//called as automorphic number
//example : N=5,N*N=25 the last digit 5 is equal to the number itself 5 thus it is automorphic
//N=76, N*N=5776  

class automorphic{
	public static void main(String[] args){

		int N=76,count=0,count2=0;
		int temp=N*N;

		while(N!=0){
			count++;
			int rem1=N%10;
			int rem2=temp%10;

			if(rem1==rem2){
				count2++;
			}
			N=N/10;
			temp=temp/10;
		}

		if(count==count2)
			System.out.println("Automorphic");
		else
			System.out.println("Not automorphic");
	}
}
