//A perfect number is a number where the sum of the factors excluding the number itself is equal to the number.

class perfect{
	public static void main(String[] args){
		int N=6,sum=0;

		for(int i=1;i<=N/2;i++){
			if(N%i==0)
				sum=sum+i;
		}

		if(sum==N)
			System.out.println("Perfect number");
		else
			System.out.println("Not a perfect number");
	}
}
