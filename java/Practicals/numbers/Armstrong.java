//Armstrong number is a number in which the sum of the digits raised to the power of total number of digits in that 
//number is equal to the number.
//example: 153 expln: 1*1*1+5*5*5+3*3*3 = 153
//1634 : 1*1*1*1+6*6*6*6+3*3*3*3+4*4*4*4

class armstrong{
	public static void main(String[] args){

		int N=147;
		int count=0,temp1=N,temp2=N;

		while(N!=0){
			count++;
			N=N/10;
		}
		
		int sum=0;

		while(temp1!=0){
			int rem=temp1%10;
			int pro=1;
			for(int i=1;i<=count;i++){
				pro=pro*rem;
			}
			sum=sum+pro;
			temp1=temp1/10;
		}

		if(temp2==sum){
			System.out.println("Armstrong number");
		}else{
			System.out.println("NOt an armstrong number");
		}
	}
}

