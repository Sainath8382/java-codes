//A composite number is a number which has more than 2 factors

class composite{
	public static void main(String[] s){

		int N=6;
		int count=0;
		int flag=0;

		for(int i=1;i<=N;i++){
			if(N%i==0)
				count++;
			if(count>2){
				flag=1;
				break;
			}
		}
		if(flag==1)
			System.out.println("Composite number");
		else
			System.out.println("Not a composite number");
	}
}	
