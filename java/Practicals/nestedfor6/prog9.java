//Q9
//write a program to print a series of strong numbers from entered range. ( Take a start and end number
//from a user )
//Input:-
//Enter starting number: 1
//Enter ending number: 150
//Output:-
//Output: strong numbers between 1 and 150
//1 2 145

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int start=Integer.parseInt(br.readLine());
		int end = Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){

			int temp1=i;
			int sum=0;
			while(temp1!=0){

				int fact=1;
				int digit = temp1%10;
				for(int j=1;j<=digit;j++){
					fact = fact*j;
				}
					sum = sum+fact;
				
				temp1=temp1/10;
			}
			
			if(sum==i)
				System.out.println(i);
			}
		}
	}




