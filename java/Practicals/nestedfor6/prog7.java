//Q7
//Write a program to take range as input from the user and print the reverse of all numbers. ( Take a
//start and end number from a user )
//Input: Enter start: 100
//Enter end: 200

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter start and end");
		int start = Integer.parseInt(br.readLine());
		int end = Integer.parseInt(br.readLine());

		for(int i=start ;i<=end;i++){

			int temp1=i;
			int sum=0;
			
			while(temp1!=0){
				sum = sum*10+temp1%10;
				temp1=temp1/10;
			}
			System.out.println(sum);
		}
	}
}
