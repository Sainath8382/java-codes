//Q10
//Write a program to take range as input from the user and print Armstrong numbers. ( Take a start and
//end number from a user )
//Input: Enter start: 1
//Enter end: 1650
//Output: Armstrong numbers between 1 and 1650
//1 2 3 4 5 6 7 8 9 153 370 371 407 1634

import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter start and end");
		int start = Integer.parseInt(br.readLine());
		int end= Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){

			int temp1=i,temp2=i;
			int count=0;
			while(temp1!=0){
				count++;
				temp1=temp1/10;
			}

			int sum = 0;
			while(temp2!=0){
				int digit = temp2%10;
				int pro =  1;
				for(int j=1;j<=count;j++){
					pro = pro*digit;
				}
				sum = sum+pro;
				temp2 = temp2/10;
			
			}
			if(sum == i)
				System.out.println(i);
			
		}
	}
}


