//Count odd digits of number

class odd{
	public static void main(String[] args){
		int N=942111423;
		int count=0,val=0;
		
		if(N<0){
			System.out.println("Wrong input");
		}
		while(N!=0){
			val=N%10;
			if(val%2!=0)
				count++;

			N=N/10;
		}
		System.out.println(count);
	}
}

