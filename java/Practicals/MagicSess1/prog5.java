//print square of even nos from the given integer value

class square{
	public static void main(String[] args) {
		int N=942111423;
		int var;
		while(N!=0){
			var=N%10;
			if(var%2==0){
				System.out.println(var*var);
			}
			N=N/10;
		}
	}
}
