//Input: Enter start number - 2
//Enter End number - 9
//Output:
//8 6 4 2
//3 5 7 9

import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start and end");
		int s = Integer.parseInt(br.readLine());
		int e = Integer.parseInt(br.readLine());

		for(int i=s;i<=e;i++){

			if(i%2!=0)
				System.out.print(i+"\t");
		}
		
		System.out.println();

		for(int j=e;j>=s;j--){

			if(j%2==0)
				System.out.print(j+"\t");
		}

	}
}
