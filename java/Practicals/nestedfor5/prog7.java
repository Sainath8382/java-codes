//write a program to print the following pattern
//Row =5;
//O
//14 13
//L K J
//9 8 7 6
//E D C B A
//
//Row = 4
//10
//I H
//7 6 5
//D C B A

import java.io.*;

class Demo{

	public static void main(String[] s)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Rows");
		int rows = Integer.parseInt(br.readLine());

		int num = rows*(rows+1)/2;
		char ch = (char)(64+num);

		for(int i=1;i<=rows;i++){

			if(rows%2==0){
				
				for(int j=1;j<=i;j++){
					if(i%2 !=0){
						System.out.print(num+"\t");
					}else{
						System.out.print(ch+"\t");
					}
					num--;
					ch--;
				}
			}else{

				for(int j=1;j<=i;j++){

					if(i%2==0)
						System.out.print(num+"\t");
					else
						System.out.print(ch+"\t");

					num--;
					ch--;
				}
			}
			System.out.println();
		}
	}
}































