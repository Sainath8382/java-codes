//D4 C3 B2 A1
//A1 B2 C3 D4
//D4 C3 B2 A1
//A1 B2 C3 D4

import java.io.*;

class Demo{

	public static void main(String[] s)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Rows ?");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1;i<=rows;i++){

			char ch1 = (char)(64+rows);
			char ch2 = 'A';
			int num =rows;

			for(int j=1;j<=rows;j++){

				if(i%2!=0){
					System.out.print(ch1);
					System.out.print(num+" ");
					ch1--;
					num--;
				}else{
					System.out.print(ch2);
					System.out.print(j+" ");
					ch2++;
				}
			}
			System.out.println();
		}
	}
}

