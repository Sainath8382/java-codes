//5 4 3 2 1
//8 6 4 2
//9 6 3
//8 4
//5

import java.io.*;

class Demo{

	public static void main(String[] s)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter rows");
		int rows = Integer.parseInt(br.readLine());

		int temp = rows;

		for(int i=1;i<=rows;i++){
			int num = temp*i;
			for(int j=rows;j>=i;j--){

				System.out.print(num+" ");
				num = num-i;
			}
			temp--;
			System.out.println();
		}
	}
}


