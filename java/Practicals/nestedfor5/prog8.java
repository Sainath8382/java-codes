//$
//@ @
//& & &
//# # # #
//$ $ $ $ $
//@ @ @ @ @ @
//& & & & & & &
//# # # # # # # #

import java.io.*;

class Demo{

	public static void main(String[] s) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Rows ");

		int rows = Integer.parseInt(br.readLine());

		int temp =1;

		for(int i=1;i<=rows;i++){

			for(int j=1;j<=i;j++){

				if(temp ==1){
					System.out.print("$ ");
				}else if(temp ==2){
					System.out.print("@ ");
				}else if(temp ==3){
					System.out.print("& ");
				}else if(temp==4){
					System.out.print("# ");
				}
				
			}
			temp++;
			if(temp>4){
				temp=1;
			}

			System.out.println();
		}
	}
}
