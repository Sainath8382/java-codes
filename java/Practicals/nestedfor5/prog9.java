//Write a program to take a number as input and print the Addition of Factorials of each
//digit from that number.
//Input: 1234
//Output: Addition of factorials of each digit from 1234 = 33

import java.io.*;

class Demo{

	public static void main(String[] s) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number");
		int no = Integer.parseInt(br.readLine());
		int no1=no;

		int sum =0;

		while(no !=0){

			int num = no%10;
			int fact =1;

			for(int i=1;i<=num;i++){

				fact=fact*i;
			}
			sum = sum+fact;
			no=no/10;
		}

		System.out.println("Addition of factorials of each digits of "+no1+" = "+sum);
	}
}


