//WAP to take two String input from user and check the String is 
//Anagaram or Not

import java.io.*;

class StringDemo{

	static boolean isAnagram(String str1 , String str2){

		if(str1.length() != str2.length())
			return false;
		else{
			char arr1[] = str1.toCharArray();
			char arr2[] = str2.toCharArray();

			for(int i=0; i<arr1.length ;i++){

				for(int j=0 ;j<arr1.length; j++){

					if(arr1[i] == arr2[j] || arr1[i] == (char) arr2[j]+32 || arr1[i] == (char)arr2[j] -32 || arr2[i] == (char) arr1[i]+32 || arr2[i] == (char) arr1[i] -32)
						arr2[j] = 'a';
				}
			}

			int count = 0;
			for(int i=0 ;i<arr1.length;i++){

				if(arr2[i] == 'a')
					count++;
			}

			if(count != str2.length())
				return false;

			return true;
		}
	}

	public static void main(String[] args) throws IOException{

		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter string");
		String str1 = br.readLine();
		//br.skip(1);
		String str2 = br.readLine();

		boolean isAnagram = isAnagram( str1, str2);
		if(isAnagram == true)
			System.out.println(str1 + ","+str2+ " are anagram strings");
		else
			System.out.println(str1 + ","+str2+ " are not anagram strings");


	}
}
