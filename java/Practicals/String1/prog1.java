//WAP to Take A String from User and Print the count of vowel from 
//the String.

import java.io.*;

class StringDemo{

	static int countvowel(String str){

		char carr[] = str.toCharArray();
		int count = 0;
		for(int i=0; i<carr.length;i++){

			if((carr[i] == 'A' || carr[i] == 'E' || carr[i] == 'I' || carr[i] == 'O' || carr[i] == 'U') || (carr[i] == 'a' || carr[i] == 'e' || carr[i]== 'i' || carr[i] == 'o' ||carr[i] == 'u'))
				count++;
		}

		return count;
	}

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String");
		String str = br.readLine();

		int vowel = countvowel(str);
		System.out.println(vowel);
	}
}
