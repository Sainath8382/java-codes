//WAP progrom to Take String from User And make it in ToggleCase . 
//(for e.g. Core2Web:-> cORE2wEB)
//

import java.io.*;

class StringDemo{

	public static String Toggle( String str){

		char carr[] = str.toCharArray();
		for(int i=0; i<carr.length ;i++){

			if(carr[i] >= 65 && carr[i] <= 90)
				carr[i] = (char)(carr[i] + 32);
			else
				carr[i] = (char) (carr[i] - 32);
		}

		return new String(carr);
	}

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String\n");
		String str = br.readLine();

		System.out.println(Toggle(str));
	}
}
