//WAP program to take a String from user and print the largest ASCII 
//character from it.

import java.io.*;

class StringDemo{

	static int Largest( String str){

		char carr[] = str.toCharArray();
		int largest = carr[0];

		for(int i=1;i<carr.length;i++){

			if(carr[i]>largest){
				largest = carr[i];
			}
		}

		return largest;
	}

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();

		int largest = Largest(str);
		char c = (char) largest;
		System.out.println("Largest value is "+largest+" for char "+c);
	}
}
