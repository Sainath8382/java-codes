//WAP to To take a String from user and check the String is 
//alphanumeric or not.
//

import java.io.*;

class StringDemo{

	static boolean isAlphanum( String str){

		char carr[] = str.toCharArray();

		for(int i = 0 ; i<carr.length;i++){

			if((carr[i]>=48 && carr[i]<=57) && ( (carr[i]>= 97 && carr[i]<=122) || (carr[i] >= 65 && carr[i]<= 90)))
				return true;
		}
		
			return false;
	}


	public static void main(String[] args) throws IOException{

		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter string");
		String str = br.readLine();
		
		boolean anum = isAlphanum(str);
		if(anum == true)
			System.out.println(str+" is Alphanumeric");
		else
			System.out.println(str+" is not Alphanumeric");
	}
}
