//foreach loop increments automatically.

class ForeachDemo{

	public static void main(String[] args){

		int arr[] = {10,20,30,40,50,60};

		for(float x:arr)
			System.out.println(x);
	}
}
