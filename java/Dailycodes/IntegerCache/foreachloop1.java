//foreach is an another loop in java
//Its drawback is that it only works on class
//Its other drawback is it works on multiple data,no break and continue works on foreach loop.

class ForeachDemo{

	public static void main(String[] args){

		int arr[] = {10,20,30,40,50};

		for(int x:arr)
			System.out.println(x);

	}
}
