// private instance variable scenario is that it can only be accessed in the class in which it is mentioned.
//

class C2w{

		private int x = 20;

		void disp(){			
			
			System.out.println(x);
		}
}

class Demo{

	public static void main(String[] args){

		C2w obj = new C2w();

		obj.disp();
		System.out.println(obj.x);
	}
}
