//Default access specifier scenario in which it works in same code file having multiple classes.

class C2w{

	int courses = 8;
	String favCourse = "Java Ds";

	void disp(){

		System.out.println(courses);
		System.out.println(favCourse);
	}
}

class Student{

	public static void main(String[] args){

		C2w obj = new C2w();
		obj.disp();
	}
}
