//Creating constructor in the code itself.
//we cannot add invokespecial it is added by compiler itself.

class Core2Web{

	Core2Web(){

		System.out.println("IN Constructor");
	}

	int x=10;
	public static void main(String[] args){

		System.out.println("In main");
		Core2Web obj = new Core2Web();
	}

	int y = 10;
}


