//instance variables are different for every object. i.e. the change in instance variables is for that object itself.
//
//static variables are global and the change in static variables is visible to all.

class Employee{

	int empId = 10;
	String name = "Kanha";

	static int y = 50;

	void EmpInfo(){

		System.out.println(empId);
		System.out.println(name);
		System.out.println(y);
	}
}

class MainDemo{

	public static void main(String[] args){

		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.EmpInfo();
		emp2.EmpInfo();
		System.out.println("<----------->");

		emp2.empId = 20;
		emp2.name = "Rahul";
		emp2.y = 5000;

		emp1.EmpInfo();
		emp2.EmpInfo();
	}
}
