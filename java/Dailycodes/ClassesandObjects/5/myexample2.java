//My realtime example 2

class Resort{

	String resortName = "Le Meridien...$";
	int noofRooms = 60;
	static String Services = "Spa";
	static boolean Wifi = true;

	void ResortInfo(){

		System.out.println(resortName);
		System.out.println(noofRooms);
		System.out.println(Services);
		System.out.println(Wifi );
	}
}

class Visitors{

	public static void main(String[] args){

		Resort r1 = new Resort();
		Resort r2 = new Resort();

		r1.ResortInfo();
		r2.ResortInfo();

		r1.resortName = "Westin";
		r1.noofRooms = 126;
		r1.Services = "Pool";
		r1.Wifi = false;

		r1.ResortInfo();
		r2.ResortInfo();
	}
}
