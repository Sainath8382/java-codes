//Realtime example considering a "Movie Theater"

class Theater{

	int noofScreens = 12;
	static String MovieName = "Kerela Stories";
	String TheaterName = "PVR";

	void Info(){

		System.out.println(noofScreens);
		System.out.println(MovieName);
		System.out.println(TheaterName);
	}
}

class Audience{

	public static void main(String[] args){

		Theater t1 = new Theater();
		Theater t2 = new Theater();

		t1.Info();
		t2.Info();

		t2.noofScreens = 10;
		t2.MovieName = "John-Wick";
		t2.TheaterName = "INOX";

		t1.Info();
		t2.Info();
	}
}
