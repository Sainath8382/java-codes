//Code which shows that static block comes into picture before main.

class Demo{

	static {

		System.out.println("IN static block");
	}

	public static void main(String[] args){

		System.out.println("Main_______");
	}
}
