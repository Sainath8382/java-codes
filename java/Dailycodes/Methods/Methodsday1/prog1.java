//Error associated with method
//	if the method which is called from main is not static then error occurs.
//	same is the case for variables if  global variable called from main is not static error occurs.
class Method{
	
        static int x=10;
	static int y=20;

	public static void main(String[] s){
		
		System.out.println(x);
		System.out.println(y);

		fun();
		gun();
	}

	static void fun(){

		System.out.println("In fun method");
	}

	static void gun(){

		System.out.println("In gun method");
	}
}
