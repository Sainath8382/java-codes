//error associated if the method is void and return value from the method is expected in main.

class Demo{
	
	int fun(int x){
		
		//int y = x+10;
		return x+10;
	}

	public static void main(String[] s){

		Demo obj = new Demo();
		System.out.println(obj.fun(10));
	}
}
