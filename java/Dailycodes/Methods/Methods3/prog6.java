//another way of getting inputs from method and storing it directly in one variable.

class Demo{

	void fun(int x){

		int y=x+10;
	}

	public static void main(String[] s){

		Demo obj = new Demo();
		int a = obj.fun(10);
		System.out.println(a);
	}
}


