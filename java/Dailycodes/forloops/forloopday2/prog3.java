//Armstrong number using while loop: A number whose sum of digits raised to the power of number of digits in that number is equal to that number itself is called as an Armstrong number

class armstrong{
	public static void main(String[] args){

		int N=1634;
		int var=0,sum=0,temp=N;

		while(N!=0){
			var=N%10;
			sum=sum+var*var*var*var;
			N=N/10;
		}

		if(sum==temp)
			System.out.println("Armstrong number");
		else
			System.out.println("Not an armstrong number");
	}
}

