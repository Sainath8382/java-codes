//Implementation of StringBuffer reverse function.
//return type : StringBuffer.
//parameter : StringBuffer

class myreverse{

	public static StringBuffer myreverse(String str){

		char arr[] = str.toCharArray();
		int start = 0,end = arr.length - 1;

		while(start<end){

			char temp = arr[start];
			arr[start] = arr[end];
			arr[end] = temp;

			start++;
			end--;
		}
		
		String str1 = new String(arr);
		StringBuffer str2 = new StringBuffer(str1);

		return str2;
		 
	}

	public static void main(String[] args){

		StringBuffer str1 = new StringBuffer("Sainath");
	
		StringBuffer rev = myreverse(str1.toString());

		System.out.println(rev.toString());

	}	
}

