//Implementation of StringBuffer replace method.

import java.io.*;

class myreplace{

	static String myreplace(int start,int end, String str1,String str2){

		char arr[] = str1.toCharArray();
		char replace[] = str2.toCharArray();

		for(int i=start;i<end;i++)
			arr[i] = replace[i-start];

		return new String(arr);
	}

	public static void main(String[] args){

		StringBuffer str = new StringBuffer("Sainath");
		String str1 = myreplace(3,7,str.toString(),"Amle");

		StringBuffer sb = new StringBuffer(str1);
		System.out.println(sb);
	}
}
