//Implementation of insert
//parameter : offset i.e. starting index to insert the string and String.
//return type : StringBuffer.

import java.io.*;

class myinsert{
	
	static String myinsert(String str1, int start,String str2){

		int strlength = str1.length();
		int insertlength = str2.length();
		char string[] = new char[strlength + insertlength];

		if(start<0 || start>strlength)
			System.out.println("Enter valid offset");

		for(int i = 0;i<start;i++)
			string[i] = str1.charAt(i);

		for(int i = 0;i<insertlength ;i++)
			string[start+i] = str2.charAt(i);

		for(int i=start ;i<strlength;i++)
			string[insertlength+i] = str1.charAt(i);

		return new String(string);
	}

	public static void main(String[] args){

		StringBuffer str1 = new StringBuffer("Sainath");
		
		String str2 = myinsert(str1.toString(),0,"Hello");
		StringBuffer sb = new StringBuffer(str2);
		System.out.println(sb);
	}
}
