//Implementation of the append method of the StringBuffer.

class myappend{

	static String myappend(String str1,String str2){

		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		int length = arr2.length;
		char arr3[] = new char[arr1.length+arr2.length];

		for(int i=0 ; i< arr3.length ;i++){

			if(i<arr1.length)
				arr3[i] = arr1[i];
			else
				arr3[i] = arr2[length - i];

		}

		String str = arr3.toString();
		return str;
	}

	public static void main(String[] args){

		String str1 = "Sainath";
		
		String str = myappend(str1,"Amle");
		System.out.println(str);
	}
}


