//Implementation of StringBuffer delete method.

import java.io.*;

class mydelete{

	static String mydelete(String str,int start ,int end){

		int dellength = (end-start);
		int length = str.length();
		char str1[] = new char[length-dellength];

		for(int i=0;i<start;i++)
			str1[i] = str.charAt(i);

		for(int i=end; i<length;i++)
			str1[end - dellength] = str.charAt(i);

		return new String(str1);
	}

	public static void main(String[] args){

		StringBuffer str = new StringBuffer("Sainath");
		String str2 = mydelete(str.toString(), 3,7);

		StringBuffer sb = new StringBuffer(str2);
		System.out.println(sb);
	}
}

		
