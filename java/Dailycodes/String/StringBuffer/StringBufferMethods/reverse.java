//Method to reverse the String in the StringBuffer.

// method : public synchronized StringBuffer reverse();
// return type : StringBuffer.

class reverse{

	public static void main(String[] args){
		
		StringBuffer str1 = new StringBuffer("Shashi");
		System.out.println(str1.reverse());
	}
}
