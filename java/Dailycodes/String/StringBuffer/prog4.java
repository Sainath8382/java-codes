//creating StringBuffer Object by declaring its capacity directly while creating the StringBuffer Object.
//Methods for StringBuffer declaration
//	1. StringBuffer sb = new Stringbuffer("Sainath");	capacity : 23 	(default : 16 + 7 string length
//	2. StringBuffer sb = new StringBuffer();	capacity : 16 (default)
//	3. StringBuffer sb = new StringBuffer(100);	capacity : 100
//

class SBDemo{
	public static void main(String[] args){

		StringBuffer sb = new StringBuffer(100);	
						   //^ Initial capacity
		sb.append("Biencaps");
		sb.append("Core2web");

		System.out.println(sb);
		System.out.println(sb.capacity());

		sb.append("Incubator");
		System.out.println(sb);
		System.out.println(sb.capacity());

	}
}
