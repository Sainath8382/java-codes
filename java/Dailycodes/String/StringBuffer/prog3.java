//creating a blank string object using StringBuffer and using append add multiple strings further in that blank object.
//The blank string has capacity of 16 by default even if the string is added further.It does not change its capacity which is the case if the object is 
//	created then only string is given to the object.


class StringBufferDemo{

	public static void main(String[] args){

		StringBuffer sb = new StringBuffer();
		System.out.println(sb.capacity());
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));

		
		sb.append("Shashi");
		System.out.println(sb);
		System.out.println(sb.capacity());

		sb.append("Bagal");
		System.out.println(sb.capacity());
		System.out.println(sb);

		sb.append("Core2web");
		System.out.println(sb.capacity());
		System.out.println(sb);

		System.out.println(System.identityHashCode(sb));
	}
}
