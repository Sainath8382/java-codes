//Why to use StringBuffer ?
//StringBuffer object is mutable in nature. Due to this feature if the string is to be changed further by increasing its length it can be done without creating a new object.
//when a string is created using StringBuffer then it allocates extra space for the characters while creating the object itself. But the extra space is limited.
//	If the characters are beyond the limit then new object is created
//
//The second most important reason for StringBuffer is that its methods are Synchronous.

class StringBufferDemo{

	public static void main(String[] args){

		StringBuffer str = new StringBuffer("");
		System.out.println(str.capacity());

		StringBuffer str1 = new StringBuffer("Sainath");
		System.out.println(str1);
		System.out.println(System.identityHashCode(str1));

		str1.append("Amle");

		System.out.println(str1);
		System.out.println(System.identityHashCode(str1));
		System.out.println(str1.capacity());
	}
}

//My conclusion : I have tried the string having the capacity as 317 characters but there also the object of both the strings remains the same !!
