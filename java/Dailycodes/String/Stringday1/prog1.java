//The string initialized with new is into the heap section.
//The string initialized directly such as String str1="Core2web"; then it is into String Constant Pool if a new String with the same data is there then it 
//	also points to the same str1 string.

class StringDemo{

	public static void main(String[] args){

		String str1 = "Core2web";	//String constant pool
		String str2 = new String("Core2web");	//Heap

		char str3[] = {'C','2','W'};	//Integer cache as charcter goes internally as integer.

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
	}
}
