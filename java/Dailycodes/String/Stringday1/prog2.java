
class StringDemo{

	public static void main(String[] args){

		String str1 = "Core2web";		//String Constant Pool
		String str2 = new String("Core2web");	//Heap

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
	
		String str3 = "Core2web";		//pointing to the same object of str1 in string constant pool. 
		String str4 = new String("Core2web");	//Heap again new object.

		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));

	}
}
