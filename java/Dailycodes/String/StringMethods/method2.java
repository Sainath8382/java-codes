//method : public int length();
//desc : It returns number of characters contained in given string
//parameters : none
//return type : int

class LengthDemo{

	public static void main(String[] args){

		String str1 = "Core2Web";
		System.out.println(str1.length());
		
	}
}
