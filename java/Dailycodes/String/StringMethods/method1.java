//method : public string concat(String str);
//description : - concatenate string to this string ie Another string concated with First String.
//		- Implements new character array whose length is sum of str1.length and str2.length.
//parameters : String
//return Type : String

class ConcatDemo{

	public static void main(String[] args){

		String str1 = "Core2";
		String str2 = "Web";

		String str3 = str1.concat(str2);
		System.out.println(str3);

		System.out.println(str1.concat(str2));
	}
}
