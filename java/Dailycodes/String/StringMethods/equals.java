//method : public boolean equals(Object anObject)
//desc : compare 2 strings if the strings are having all the same characters then return true otherwise return false.
//	 returns true if anObject is semantically equal to this.
//parameters : Object (anObject)
//return type : boolean

class equalsDemo{

	public static void main(String[] args){

		String str1 = "Shashi";
		String str2 = new String("Shashi");

		System.out.println(str1.equals(str2));
	}
}
