//method : public int compareTo(String str2)
//	desc: it compares str1 & str2 (case sensitive).
//		if both strings are equal then returns 0. otherwise returns the comparison 
//		
//		comparison means the difference between the 2 characters is returned.
//

class CompareToDemo{

	public static void main(String[] args){

		String str1 = "ashish";
		String str2 = "Ashish";

		System.out.println(str1.compareTo(str2));
	}
}
