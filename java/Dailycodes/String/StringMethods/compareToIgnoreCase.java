//method : int compareToIgnoreCase(String str2);
//desc : it compares 2 strings same as that of compareTo but here the case is not considered.

class compareToIgnoreCase{

	public static void main(String[] args){

		String str1 = "SHASHI";
		String str2 = "shashi";

		System.out.println(str1.compareToIgnoreCase(str2));

		String str3 = "Sainath";
		String str4 = "sxincth";
		System.out.println(str3.compareToIgnoreCase(str4));
	}
}
