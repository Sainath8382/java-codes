//method : public char charAt(index)
//desc : It returns the character located at specified index within given string.
//parameter : integer (index)
//return type : character

class charAtDemo{

	public static void main(String[] args){

		String str = "Core2Web";

		System.out.println(str.charAt(4));
		System.out.println(str.charAt(0));
	//	System.out.println(str.charAt(8));	//StringIndexOutOfBoundsException
	}
}
