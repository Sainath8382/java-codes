
class StringDemo{

	public static void main(String[] args){

		String str1 = "Shashi";
		String str2 = "Bagal";

		System.out.println(str1);
		System.out.println(str2);

		str1.concat(str2);	//Here after concat operation the object is created containing str1 and str2. This object is
					//not stored in anything thus it has no reference due to which the garbage collector takes it 
					//out of the heap and no concat string is present in str1.
		System.out.println(str1);
		System.out.println(str2);
	}
}
