//when character array is combined with string then it also considers itself as a string and calls toString() internally

class ArrayDemo{

	public static void main(String[] args){

		char arr[] = {'A','B','C'};
		System.out.println(arr);

		System.out.println("Array = "+arr);	//Explanation above
		System.out.println(arr.toString());
	}
}
