//Print the multiplication of the digits

class whileloop{
	public static void main(String[] args){
		int N=6531;
		int pro=1;
		int var=0;

		while(N>0){
			var=N%10;
			pro=pro*var;
			N=N/10;
		}
		System.out.println(pro);
	}
}
