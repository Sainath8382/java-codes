//break example for prime number code

class breakdemo{
	
	public static void main(String[] args){

		int N=15;
		int count=0;

		for(int i=1;i<=N;i++){

			if(N%i==0)
				count++;

			if(count>2)
				break;
		}

		if(count == 0)
			System.out.println("Prime number");
		else
			System.out.println("Not a prime number");
	}
}
