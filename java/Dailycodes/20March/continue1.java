//Continue in java : Continue means skip the current term and continue printing the others.

class con{
	public static void main(String[] s){

		int N=10;

		for(int i=1;i<=N;i++){

			if(i%3==0)

				continue;

			System.out.println(i);
		}
	}
}
