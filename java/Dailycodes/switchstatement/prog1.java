//2 same cases are not allowed in switch.

class SwitchDemo{

	public static void main(String[] s){

		int x=5;

		switch(x){

			case 1:
				System.out.println("One");
				break;
			case 2:
				System.out.println("Two");
				break;

			case 1:
				System.out.println("2nd -one");
				break;

			case 2:
				System.out.println("2nd - two");
		}
	}
}
