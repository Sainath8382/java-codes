//constant expression required error

class Stringdemo{

	public static void main(String[] s){

		int a=1,b=2,x=3;

		switch(x){

			case a:
				System.out.println("1");
				break;

			case b:
				System.out.println("2");
				break;

			case a+b:
				System.out.println("3");
				break;

			case a+b+b:
				System.out.println("4");
				break;

			default:
				System.out.println("Invalid");
				break;
		}
	}
}


