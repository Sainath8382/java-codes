//proof of jagged array exists : accessing the element which is not existing. If it gives Exception then it is proved that JaggedArray exists.

class Jaggedarray{

	public static void main(String[] args){

		int arr[][] = {{10,20,30},{40,50},{70}};

		for(int i=0;i<3;i++){

			for(int j=0;j<3;j++){

				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}
}
