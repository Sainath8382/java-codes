//Arrays in java

class ArrayDemo{

	public static void main(String[] s){

		int arr[];	//Array declaration in java.  This same line gives error in C : array index missing
		
		int arr[5];	//This declaration is done in C. It gives error in java.
	}
}
