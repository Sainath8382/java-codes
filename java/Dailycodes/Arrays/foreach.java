//This code is the explanation for the foreach loop and its working. For each loop requires collection of data.
//	foreach loop works when the array object is given not for any other data such as integer,float,etc.
//

class foreach{

	public static void main(String[] args){

		int arr[] = {1,2,3};
		for(int x:arr){
			System.out.println(x);
		}
		
		for(int x:3){
			System.out.println(x);
		}
	}
}
