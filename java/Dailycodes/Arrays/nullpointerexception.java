//Null pointer exception occurs in java when the 2d array is not given the data for number of columns and the length of the columns is accessed.
//	In java only rows given to a 2d array works. Further we need to peovide the column data to every row i.e. 1D array object.
//

class NullPointer{

	public static void main(String[] args){

		int arr1[][] = {{},{},{}};
		int arr2[][] = new int[2][];

		System.out.println(arr1.length);
		System.out.println(arr1[0].length);

		System.out.println(arr2.length);
	//	System.out.println(arr2[0].length);	//NullPointerException
	
	//solution for the exception
		arr2[0] = new int[4];
		System.out.println(arr2[0].length);
	}
}
