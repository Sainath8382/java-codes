//Understanding the internal memory given to an array on the heap section using System.identityHashCode

class Array{

	public static void main(String[] args){

		int arr[] = {10,20,30,40};
		int arr1[] = {10,20,30,40};

		for(int i=0;i<arr.length;i++){

			System.out.println("arr[i] = "+ System.identityHashCode(arr[i]));
			System.out.println("arr1[i] = "+System.identityHashCode(arr1[i]));
		}

		System.out.println(System.identityHashCode(arr));
		System.out.println(System.identityHashCode(arr1));
		
		System.out.println(arr);
		System.out.println(arr1);
	}
}	
