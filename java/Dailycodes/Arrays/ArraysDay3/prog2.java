
import java.io.*;

class Array{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int size;
		System.out.println("Enter array size");
		size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int count =0;

		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());

			if(arr[i]%2==0)
				count++;

		}

		System.out.println("Even elements : "+count);
	}
}

