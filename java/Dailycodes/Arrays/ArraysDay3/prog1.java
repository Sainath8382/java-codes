
import java.io.*;

class Array{

	public static void main(String[] args) throws IOException{

	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	int arr[] = new int[5];

	int add = 0;
	System.out.println("Enter array elements:");
	for(int i=0;i<arr.length;i++){
		
		arr[i] = Integer.parseInt(br.readLine());
		add = add+arr[i];
	}

	System.out.println("Sum of array elements : "+add);
	}
}
