//printing multiple tokens using while loop and StringTokenizer methods 
//this is not a proper way of printing different data types 

import java.util.*;

class StringDemo{

	public static void main(String[] s){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter player info\n");
		String str = sc.nextLine();

		StringTokenizer st = new StringTokenizer(str," ");

		System.out.println(st.countTokens());

		while(st.hasMoreTokens()){

			System.out.println(st.nextToken());
		}

	}
}
