//StringTokenizer : It is used to divide the entire input into multiple parts i.e. Tokens. This is used for inputs to be taken from 
//	a single line.

import java.io.*;
import java.util.*;

class StringDemo{

	public static void main(String[] s) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENter society name,wing and flat");
		String info = br.readLine();

		StringTokenizer obj = new StringTokenizer(info," ");
		String token1 = obj.nextToken();
		String token2 = obj.nextToken();
		String token3 = obj.nextToken();

		System.out.println("Society name = "+token1);
		System.out.println("Wing = "+token2);
		System.out.println("flat = "+token3);

	}
}
