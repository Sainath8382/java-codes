//StringTokenizer Realtime example

import java.io.*;
import java.util.*;

class Demo{

	public static void main(String[] s) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter movie info ");
		String info = br.readLine();

		StringTokenizer obj = new StringTokenizer (info," ");

		String token1 = obj.nextToken();
		char token2 = obj.token2.charAt(0);

		int token3 = Integer.parseInt(obj.nextToken());
		float token4 = Float.parseFloat(obj.nextToken());

		System.out.println("Movie name = "+token1);
		System.out.println("Auditorium = "+token2);
		System.out.println("person = "+token3);
		System.out.println("Price = "+token4);

	}
}
