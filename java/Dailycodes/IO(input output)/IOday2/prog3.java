//Taking character input between 2 different inputs creates problem which is the input after the character cannot be taken.
//The tempprary solution for this problem is to use br.skip().

import java.io.*;

class Society{

	public static void main(String[] s)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
		System.out.println("Enter name :");
		String name = br.readLine();

		System.out.println("Enter wing name :");
		char wing = (char)br.read();
		br.skip(1);

		System.out.println("Enter flatno :");
		int flat = Integer.parseInt(br.readLine());

		System.out.println("Soc name ="+name);
		System.out.println("Wing ="+wing);
		System.out.println("Flat ="+flat);
		
		
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
		br.close();
		System.out.println("Enter name :");
		String name2 = br.readLine();
	}
}
		
