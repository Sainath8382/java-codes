//int,float and string inputs by using BufferedReader

import java.io.*;

class Player{

	public static void main(String[] s)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENter player name :");
		String name= br.readLine();

		System.out.println("ENter jersey no :");
		int jerNo= Integer.parseInt(br.readLine());

		System.out.println("ENter average :");
		float avg= Float.parseFloat(br.readLine());

		System.out.println("Player = "+name);
		System.out.println("Jerno = "+jerNo);
		System.out.println("Avg = "+avg);
	}
}
