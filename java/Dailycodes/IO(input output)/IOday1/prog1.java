//Scanner code : The errors associated with usage of Scanner.
//Here there are 2 errors for using Scanner without importing the Scanner class.
class ScannerDemo{

	public static void main(String[] s){

		Scanner obj = new Scanner(System.in);

		System.out.println("Enter name : ");
		String name = obj.next();

		System.out.println(name);
	}
}
