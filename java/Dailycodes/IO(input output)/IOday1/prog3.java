//Buffered Reader : Buffered reader uses buffer for taking inputs. This buffer can store only 1 character
//	at a time. If you want only one character as input then only buffer will work.
//Whereas for inputs with multiple characters there is need of Input Stream Reader it forces the Buffered
//Reader to take inputs with more than 1 characters and entire line can be read and taken as input.

//Errors which can occur while using Buffered Reader are : You should import it from io package as 
//import java.io.*;
//It throws exception then either you must handle it or mention in Code as throws IOException(which means declare it
//	to be thrown).

import java.io.*;

class InputDemo{

	public static void main(String[] s)throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter name :");
		String name = br.readLine();

		System.out.println("Enter your age: ");
		int age = Integer.parseInt(br.readLine());

		System.out.println(name);
		System.out.println(age);
	}
}

