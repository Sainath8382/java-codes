//If else ladder

class ifelse{
	public static void main(String[] args){
		float temp=98.4f;

		if(temp>98.6f){
			System.out.println("High");
		}else if(temp<98.0){
			System.out.println("Low");
		}else{
			System.out.println("Normal");
		}
	}
}
