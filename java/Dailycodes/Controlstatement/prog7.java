//print fizz - divisible by 3
//buzz - divisible by 5
//fizz-buzz if by both
//not by both

class ifelse{
	public static void main(String[] args){
		int x=30;
		if(x%3==0 && x%5==0)
			System.out.println("fizz buzz");
		else if(x%3==0)
			System.out.println("fizz");
		else if(x%5==0)
			System.out.println("buzz");
		else
			System.out.println("Not divisible by both");
	}
}
